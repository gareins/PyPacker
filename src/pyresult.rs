use backtrace::Backtrace;
use std::io;
use std::fmt;
use walkdir;
use std::error::Error;
use curl;

pub struct PyError {
    pub back: Backtrace,
    pub more: String
}

impl PyError {
    pub fn new(msg: String) -> PyError {
        PyError {
            back: Backtrace::new(),
            more: String::from(msg)
        }
    }
}

impl From<io::Error> for PyError {
    fn from(e: io::Error) -> PyError {
        PyError::new(String::from(e.description()))
    }
}
impl From<String> for PyError {
    fn from(e: String) -> PyError {
        PyError::new(e)
    }
}
impl From<&'static str> for PyError {
    fn from(e: &'static str) -> PyError {
        PyError::new(String::from(e))
    }
}
impl From<walkdir::Error> for PyError {
    fn from(e: walkdir::Error) -> PyError {
        PyError::new(format!("Traversing directory: {}", e))
    }
}
impl From<curl::Error> for PyError {
    fn from(e: curl::Error) -> PyError {
        PyError::new(String::from(e.description()))
    }
}

impl fmt::Display for PyError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "ERR: {}\n{:?}", self.more, self.back)
    }
}

pub type PyResult = Result<(), PyError>;
