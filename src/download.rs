use std::path::Path;
use std::fs;
use std::os;
use std::io;
use std::env;
use std::collections::HashMap;

use curl::easy::Easy;
use curl;
use bzip2;
use tar;
use pyresult::{PyError, PyResult};
use walkdir;

macro_rules! targets {
    () => {{
        let mut m: HashMap<Target, &'static str> = HashMap::new();
        m.insert(Target::new(3, "linux", "amd64"), "https://bitbucket.org/squeaky/portable-pypy/downloads/pypy3.3-5.5-alpha-20161013-linux_x86_64-portable.tar.bz2");
        m.insert(Target::new(3, "linux", "i386"), "https://bitbucket.org/squeaky/portable-pypy/downloads/pypy3.3-5.5-alpha-20161014-linux_i686-portable.tar.bz2");
        m.insert(Target::new(2, "linux", "amd64"), "https://bitbucket.org/squeaky/portable-pypy/downloads/pypy-5.4.1-linux_x86_64-portable.tar.bz2");
        m.insert(Target::new(2, "linux", "i386"), "https://bitbucket.org/squeaky/portable-pypy/downloads/pypy-5.4.1-linux_i686-portable.tar.bz2");
        m
    }}
}


#[derive(PartialEq, Eq, Hash)]
struct Target {
    pub version: i32,
    pub platform: String,
    pub arch: String
}

impl Target {
    fn new(ver: i32, platform: &str, arch: &str) -> Target {
        Target {
            version: ver,
            platform: String::from(platform),
            arch: String::from(arch),
        }
    }
    fn desc(&self) -> String {
        format!("{}-{}-{}", self.version, self.platform, self.arch)
    }
}

fn get_url(t: &Target) -> Option<&str> {
    let targets = targets!();
    if targets.get(t).is_some() {
        Some(targets[t])
    } else {
        None
    }
}

pub fn list_targets() -> String {
    // i wrote this one liner correct in my first try. Yeah, baby :)
    targets!().keys().map(|x| x.desc()).fold(String::new(), |s, x| format!("{}{}\n", s, x))
}

pub fn download_ans_extract(ver: i32, platform: &str, arch: &str, folder: &Path) -> PyResult {
    let target = Target::new(ver, platform, arch);
    let _f = format!("../pypy-{}", target.desc());
    let tmp_folder = Path::new(&_f);

    if !tmp_folder.is_dir() {
        let url = get_url(&target);
        if url.is_none() {
            return Err(PyError::from(format!("Unsupported target: {}", target.desc())));
        }
        let url = url.unwrap();

        println!("* Downloading");
        let pypy_interpreter = try!(download(url));

        try!(extract(pypy_interpreter, tmp_folder));
    }

    try!(copy_dir(tmp_folder, folder));
    Ok(())
}

fn download(url: &str) -> Result<Vec<u8>, curl::Error> {
    let mut buf = Vec::new();
    let mut handle = Easy::new();

    try!(handle.progress(true));
    try!(handle.follow_location(true));
    try!(handle.url(url));
    {
        let mut transfer = handle.transfer();
        try!(transfer.write_function(|data| {
            buf.extend_from_slice(data);
            Ok(data.len())
        }));
        try!(transfer.perform());
    }

    Ok(buf)
}

fn extract(file_content: Vec<u8>, out_folder: &Path) -> PyResult {
    let bz2_decompressed = bzip2::decompress(&file_content);
    let mut bz2_slice: &[u8] = &bz2_decompressed;

    let mut t = tar::Archive::new(&mut bz2_slice);
    let tmp_folder = Path::new("__tmp_pypy");
    try!(t.unpack(tmp_folder));
    if let Some(iter) = fs::read_dir(tmp_folder).unwrap().next() {
        let tar_upper_folder = iter.unwrap().file_name().into_string().unwrap();
        let from_folder = format!("{}/{}", tmp_folder.display(), tar_upper_folder);

        try!(fs::rename(from_folder, out_folder));
        try!(fs::remove_dir_all(tmp_folder));
        Ok(())
    } else {
        Err(PyError::from("No data found in interpreter tar."))
    }
}

fn copy_dir(from: &Path, to: &Path) -> PyResult {
    let l = format!("{}", from.display()).len();
    for entry in walkdir::WalkDir::new(from) {
        let walkdir_entry = try!(entry);
        let entry = walkdir_entry.path();

        let source = format!("{}", entry.display());
        let dest = format!("{}{}", to.display(), &source[l..]);

        if entry.is_dir() {
            try!(fs::create_dir_all(dest));
        }
        else if entry.is_file() {
            let p1 = try!(abs_path(&source));
            let p2 = try!(abs_path(&dest));
            try!(os::unix::fs::symlink(p1, p2));
        }
    }
    Ok(())
}

pub fn abs_path(p: &str) -> io::Result<String> {
    let pwd = try!(env::current_dir());
    Ok(format!("{}/{}", pwd.display(), p))
}
