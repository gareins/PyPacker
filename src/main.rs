extern crate zstd;
extern crate tar;
extern crate argparse;
extern crate time;
extern crate bzip2;
extern crate walkdir;
extern crate backtrace;
extern crate curl;

// mods
mod download;
mod pyresult;

// standard lib
use std::process;
use std::process::{Command, Stdio};
use std::fs;
use std::os;
use std::io;
use std::path::Path;
use std::env;
use std::cell::RefCell;

use argparse::{ArgumentParser, Store, StoreFalse, StoreTrue, Print};
use pyresult::{PyError, PyResult};
use download::abs_path;

// traits
use std::io::Write;


macro_rules! verbose {
    ($v:expr, $fmt:expr) => {
        if $v {
            println!($fmt);
        }
    };
    ($v:expr, $fmt:expr, $($arg:tt)*) => {
        if $v {
            println!( $fmt, $($arg)*);
        }
    }
}


struct Options {
    pub target: String,
    pub pkg: String,
    pub main: String, // TODO: optional (if only one file...)

    pub verbose: bool,
    pub zstd: u32,
    pub keep: bool,
    pub debug: bool,
    pub module: String,
}

impl Options {
    pub fn new() -> Options {
        Options {
            target: String::from(""),
            pkg: String::from(""),
            main: String::from(""),
            verbose: true,
            zstd: 10,
            keep: false,
            debug: false,
            module: String::from(""),
        }
    }
}

struct Cleaner {
    pub keep: bool,
}

impl Drop for Cleaner {
    fn drop(&mut self) {
        if !self.keep {
            if let Ok(_) = env::set_current_dir("..") {
                fs::remove_dir_all(Path::new("build")).unwrap_or(());
            }
        }
    }
}

fn main() {
    let mut opts = Options::new();
    {
        let mut ap = ArgumentParser::new();
        ap.set_description("Pack python application with Pypy.");
        ap.refer(&mut opts.target)
            .required()
            .add_argument("target",
                          Store,
                          "target to build for, <python_version>-<os>-<architecture>. Example: \
                           3-linux-amd64");
        ap.refer(&mut opts.pkg)
            .required()
            .add_argument("package", Store, "python package location");
        ap.refer(&mut opts.main)
            .required()
            .add_argument("main_py", Store, "file that will be run with pypy");
        ap.refer(&mut opts.verbose)
            .add_option(&["-v", "--verbose"], StoreTrue, "Be verbose")
            .add_option(&["-q", "--quiet"], StoreFalse, "Be verbose");
        ap.refer(&mut opts.keep)
            .add_option(&["-k", "--keep"],
                        StoreTrue,
                        "Keep temporary files generated at build");
        ap.refer(&mut opts.module)
            .add_option(&["-m", "--module"],
                        Store,
                        "Module name inside python package. Default: $package");
        ap.refer(&mut opts.zstd)
            .add_option(&["-z", "--zstd"],
                        Store,
                        "Zstd compression level (1-21). Default: 10");
        ap.refer(&mut opts.debug)
            .add_option(&["-d", "--debug"],
                        StoreTrue,
                        "bargo build debug for runner.");
        ap.add_option(&["-l", "--list"], Print(download::list_targets()), "List avaliable targets");

        ap.parse_args_or_exit();
    }

    if let Err(e) = packer(opts) {
        println!("{}", e);
        process::exit(1);
    } else {
        println!("DONE.");
    }
}

fn packer(opts: Options) -> PyResult {
    try!(check_for_required_binaries());

    let pwd_path = try!(env::current_dir());
    let pwd = format!("{}", pwd_path.display());

    let target = opts.target; //String::from("3-linux-amd64");
    let pkg = opts.pkg; //"../../examples/hello_world";
    let main_py = opts.main; //"main.py";
    let v = opts.verbose;

    let pkg_split = pkg.split("/").collect::<Vec<&str>>();
    let pkg_name = pkg_split.last().unwrap();

    verbose!(v, "* Preparing build folder");
    try!(prepare_and_chdir_to_build(&target));
    let _ = Cleaner { keep: opts.keep }; // used to cleanup at drop()

    let pkg_loc = format!("{}/{}", pwd, pkg);
    if !Path::new(&pkg_loc).is_dir() {
        return Err(PyError::from("Cannot find package"));
    }

    try!(make_root());
    let root_abs = try!(abs_path("root"));

    verbose!(v, "* Installing dependencies");
    try!(install_dependencies(&pkg_loc));

    verbose!(v, "* Installing package: {}", pkg);
    try!(env::set_current_dir(Path::new(&pkg_loc)));
    let cmd = vec!["python", "setup.py", "install", "--root", &root_abs];
    try!(run_cmd(cmd));
    let cmd = vec!["python", "setup.py", "clean", "--all"];
    try!(run_cmd(cmd));

    let build_path = format!("{}/../", &root_abs);
    try!(env::set_current_dir(Path::new(&build_path)));

    verbose!(v, "* Creating tar");
    let tar_bytes_cell = RefCell::new(Vec::new());
    try!(to_tar(&tar_bytes_cell));
    let tar_bytes = tar_bytes_cell.into_inner();

    verbose!(v, "* Compress with zstd");
    try!(compress(&tar_bytes, "pkg.zst", &opts.zstd));

    verbose!(v, "* Writing rest build data");
    try!(write_build_data(&pkg_name, &main_py, &opts.module));

    verbose!(v, "* Building executable");
    try!(build_runner(&opts.debug));

    verbose!(v, "* Copy executable to $PWD");
    let exec = format!("{}/{}", pwd, pkg_name);
    let exec_src = format!("runner/target/{}/runner", if opts.debug { "debug" } else { "release" });
    try!(fs::copy(Path::new(&exec_src), Path::new(&exec)));

    Ok(())
}

fn _check_bin(bin: &str) -> PyResult {
    // I cant seem to get type/hash to work :S
    let output = try!(Command::new("which").arg(bin).output());
    if output.status.success() {
        Ok(())
    } else {
        Err(PyError::from(format!("Cannot find binary {}", bin)))
    }
}

fn check_for_required_binaries() -> PyResult {
    let binaries = vec!["cargo", "pip"];
    for bin in binaries {
        try!(_check_bin(bin))
    }
    Ok(())
}

fn compress(data: &[u8], name: &str, zstd: &u32) -> PyResult {
    let zstd_level: i32 = *zstd as i32;
    let mut encoder = {
        let target = try!(fs::File::create(name));
        try!(zstd::Encoder::new(target, zstd_level))
    };

    try!(encoder.write(data));
    try!(encoder.finish());
    Ok(())
}

fn prepare_and_chdir_to_build(interpreter: &str) -> PyResult {
    try!(_make_dirs_and_chdir());
    try!(_create_runner());

    let cnfg = interpreter.split("-").collect::<Vec<&str>>();
    if cnfg.len() != 3 {
        return Err(PyError::new(format!("Bad interpreter config: {}", interpreter)));
    }
    let ver = match cnfg[0] {
        "2" => 2,
        "3" => 3,
        v => {
            return Err(PyError::from(format!("Bad interpreter vecsion: {}", v)));
        }
    };
    let platform = cnfg[1];
    let arch = cnfg[2];

    try!(download::download_ans_extract(ver, platform, arch, Path::new("iter")));
    Ok(())
}

fn _make_dirs_and_chdir() -> PyResult {
    let path = Path::new("build");
    if !path.exists() {
        try!(fs::create_dir("build"));
    }

    try!(env::set_current_dir(path));
    let now = time::get_time();
    let tmp_folder = format!("{}_{}", now.sec % (3600 * 24), (now.nsec / 1000) as u32);
    try!(fs::create_dir(&tmp_folder));
    try!(env::set_current_dir(&tmp_folder));

    Ok(())
}

fn _create_runner() -> PyResult {
    if !Path::new("../runner").is_dir() {
        try!(fs::create_dir_all("../runner/src"));

        let runner_cargo_toml = include_bytes!("../runner/Cargo.toml");
        let mut f = try!(fs::File::create("../runner/Cargo.toml"));
        try!(f.write(runner_cargo_toml));

        let runner_src = include_bytes!("../runner/src/main.rs");
        let mut f = try!(fs::File::create("../runner/src/main.rs"));
        try!(f.write(runner_src));
    }

    let sp_from = try!(abs_path("../runner"));
    let sp_to = try!(abs_path("runner"));
    try!(os::unix::fs::symlink(sp_from, sp_to));

    Ok(())
}

fn make_root() -> PyResult {
    try!(fs::create_dir_all(format!("root/usr/lib/python3.5"))); // TODO: version agnostic

    let sp_from = try!(abs_path("iter/site-packages"));
    let sp_to = try!(abs_path("root/usr/lib/python3.5/site-packages"));
    try!(os::unix::fs::symlink(sp_from, sp_to));

    let inc_from = try!(abs_path("iter/include"));
    let inc_to = try!(abs_path("root/include"));
    try!(os::unix::fs::symlink(inc_from, inc_to));

    let bin_from = try!(abs_path("iter/bin"));
    let bin_to = try!(abs_path("root/bin"));
    try!(os::unix::fs::symlink(bin_from, bin_to));

    Ok(())
}

// try!
fn run_cmd(cmd: Vec<&str>) -> io::Result<bool> {
    let command = &cmd[0];
    let arguments = &cmd[1..];

    let mut child = try!(Command::new(command)
        .args(arguments)
        .stdout(Stdio::inherit())
        .stderr(Stdio::inherit())
        .spawn());

    let exit_code = try!(child.wait());
    Ok(exit_code.success())
}

fn install_dependencies(pkg_loc: &str) -> PyResult {
    let req = format!("{}/requirements.txt", pkg_loc);
    let cmd = vec!["pip", "install", "-I", "--root", "root", "-r", &req];

    if !Path::new(&req).is_file() {
        return Ok(());
    }

    match try!(run_cmd(cmd)) {
        true => Ok(()),
        false => Err(PyError::from("Pip unsuccessful")),
    }
}

fn to_tar(copy_to: &RefCell<Vec<u8>>) -> PyResult {
    let ref mut vec = *copy_to.borrow_mut();
    let mut ar = tar::Builder::new(vec);

    for entry in walkdir::WalkDir::new("iter") {
        let walkdir_entry = try!(entry);
        let entry = walkdir_entry.path();
        if entry.is_dir() {
            continue;
        }

        let mut f = try!(fs::File::open(entry));
        let fname = format!("{}", entry.display());

        try!(ar.append_file(fname, &mut f));
    }

    try!(ar.into_inner());
    Ok(())
}

fn write_build_data(pkg_name: &str, main_py: &str, module: &str) -> PyResult {
    // runner must exist because _create_runner() was called!
    if Path::new("runner/main.info").is_file() {
        try!(fs::remove_file("runner/main.info"));
    }
    if Path::new("runner/pkg.zst").is_file() {
        try!(fs::remove_file("runner/pkg.zst"));
    }

    let mut f = try!(fs::File::create("runner/main.info"));

    try!(f.write(main_py.as_bytes()));
    try!(f.write("\n".as_bytes()));
    try!(f.write(pkg_name.as_bytes()));

    let m = if module.len() > 0 { module } else { pkg_name };
    try!(f.write("\n".as_bytes()));
    try!(f.write(m.as_bytes()));

    try!(fs::rename("pkg.zst", "runner/pkg.zst"));
    Ok(())
}

fn build_runner(debug: &bool) -> PyResult {
    let previous = try!(env::current_dir());
    try!(env::set_current_dir(Path::new("runner")));

    let args = if *debug { vec!["build"] } else { vec!["build", "--release"] };
    let mut child = try!(Command::new("cargo")
        .args(&args)
        .stdout(Stdio::inherit())
        .stderr(Stdio::inherit())
        .spawn());

    let exit_code = try!(child.wait());
    if !exit_code.success() {
        Err(PyError::from("`Cargo build #runner` returned non zero"))
    } else {
        try!(env::set_current_dir(previous));
        Ok(())
    }
}
