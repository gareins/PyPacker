# Attempt to create binary from py files with pypy and zstandard.

Currently, binary size is ~25MB for hello world, should go down a bit, benchmarks will be posted later.

Also, hopefully, someday you will be able to choose interpreter and target os/architecture (pypy2/3, arm, windows,...).

Currently, nothing yet works, when it will, everything shall be posted right here :).

## TODO

Before public:

* script to check includes
* redo this readme
* add example for GUI app
* add tests for --keep and --debug
* document code
* split to some modules

After public:

* optimize runner for size (malloc, strip, maybe musl someday)
* benchmarks
* add argument size-optimization
* pass through signals
* pypy stripper
* cross compilation in rust
* numpy??
