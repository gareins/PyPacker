import sys

if __name__ == "__main__":
    print(sys.stdin.read(), end="")
    if len(sys.argv) > 2:
        print(sys.argv[2], file=sys.stderr, end="")
