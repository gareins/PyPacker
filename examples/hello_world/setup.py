#!/usr/bin/env python

from distutils.core import setup

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setup(name='HelloWorld',
      version='0.1',
      packages=['hello_world'],
      )
