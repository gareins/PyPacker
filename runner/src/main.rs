extern crate zstd;
extern crate tar;
extern crate tempdir;

use std::io;
use std::cell::RefCell;
use std::process::{Command, Stdio, exit};
use std::path::Path;
use std::env;
use std::iter::Iterator;

macro_rules! try_main {
    ($t:expr) => { match $t {
        Err(e) => {
            println!("ERR: {}", e);
            exit(73);
        }
        Ok(r) => r
    }
}}
macro_rules! ioerr {
    ($e:expr) => { Err(io::Error::new(io::ErrorKind::Other, $e)) }
}

fn main() {
    let pkg_zstd = include_bytes!("../pkg.zst");
    let settings_file = include_str!("../main.info");
    let settings_lines = settings_file.split("\n").collect::<Vec<&str>>();
    let main_py = settings_lines[0];
    // let pkg_name = settings_lines[1];
    let mod_name = settings_lines[2];

    let pkg_tar_rc = RefCell::new(Vec::new());
    try_main!(unzstd(pkg_zstd, &pkg_tar_rc));
    let pkg_tar = pkg_tar_rc.into_inner();

    let tmpdir_obj = try_main!(tempdir::TempDir::new("pypypacker"));
    let tmpdir = format!("{}", tmpdir_obj.path().display());
    try_main!(untar(pkg_tar, &tmpdir));

    let ret_code = try_main!(run_program(&tmpdir_obj.path(), mod_name, main_py));
    exit(ret_code);
}

fn unzstd(f: &[u8], out: &RefCell<Vec<u8>>) -> io::Result<()> {
    let mut decoder = try!(zstd::Decoder::new(f));
    try!(io::copy(&mut decoder, &mut *out.borrow_mut()));
    Ok(())
}

fn untar(t: Vec<u8>, f: &str) -> io::Result<()> {
    let mut arc = tar::Archive::new(t.as_slice());
    try!(arc.unpack(f));
    Ok(())
}

fn run_program(path: &Path, pkg_name: &str, main_py: &str) -> io::Result<i32> {
    let path = path.join(Path::new("iter"))
                   .join(Path::new("site-packages"))
                   .join(Path::new(pkg_name));

    let main_path = path.join(Path::new(main_py));
    if !path.is_dir() {
        ioerr!(format!("Cannot find iter/site-packages/{} folder extracted from package tar", pkg_name))
    } else if !main_path.is_file() {
        ioerr!(format!("Cannot find {} file in package {} ({})", main_py, pkg_name, main_path.display()))
    } else if !env::set_current_dir(path).is_ok() {
        ioerr!(format!("Cannot cd into {}", pkg_name))
    } else {
        let args: Vec<String> = std::env::args().collect();

        let mut child = try!(Command::new("../../bin/pypy3")
            .arg(main_py)
            .args(&args[..])
            .stdin(Stdio::inherit())
            .stdout(Stdio::inherit())
            .stderr(Stdio::inherit())
            .spawn());

        let exit_code = try!(child.wait());
        Ok(exit_code.code().unwrap_or(-1i32))
    }
}
