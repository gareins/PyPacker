#!/bin/python
import subprocess
import os
import json
import sys
import shutil
import time

def run_cmd(cmd, cwd=None):
    with subprocess.Popen(cmd, cwd=cwd, stdout=subprocess.PIPE, stderr=subprocess.PIPE) as p:
        for line in p.stdout:
            print(line.decode("utf-8"), file=sys.stderr, end="", flush=True)

        err = p.stderr.read()
        if len(err) > 0:
            print(err.decode("utf-8"), file=sys.stderr, end="", flush=True)

        return p.poll() == 0

if __name__ == "__main__":
    os.chdir(os.path.dirname(os.path.realpath(__file__)))
    exs = "../examples/"

    if not run_cmd(["cargo", "build"], cwd="../"):
        print("cargo build packer unsuccessfull")
        sys.exit(1)

    def horizontal():
        print("+" + "-" * 20 + "+---+-------+-------+")

    print("S = settings in test.json")
    horizontal()
    print("| {:18s} | S | build |  run  |".format("example name"))
    horizontal()

    # TODO: support stdin, stderr, show size
    packer = "../target/debug/pypypacker"
    for example in os.listdir(exs):
        directory = exs + example
        if "test.json" in os.listdir(directory):
            print("| {:18s} | ".format(example), end="", flush=True)
            with open(directory + "/test.json", "r") as fp:
                try:
                    dat = json.load(fp)
                except json.JSONDecodeError as e:
                    print("cannot decode test.json", file=sys.stderr)
                    print("X |       |       |")
                    continue

                if "main" not in dat:
                    print("main not in dat", file=sys.stderr)
                    print("X |       |       |")
                    continue
                else:
                    print("Y | ", end="", flush=True)

                mainpy = dat["main"]
                stdout = dat["stdout"] if "stdout" in dat else ""
                stderr = dat["stderr"] if "stderr" in dat else ""
                stdin = dat["stdin"] if "stdin" in dat else ""
                args = dat["args"] if "args" in dat else []
                pkg = [] if "pkg" not in dat else ["-m", dat["pkg"]]

                start = time.time()
                succ = run_cmd([packer, "3-linux-amd64", directory, mainpy,
                                "-z", "0", "--keep", "--debug"] + pkg)
                end = time.time()
                if not succ:
                    print("  X   |       |")
                    continue
                else:
                    print("{:2.2f} | ".format(end - start).rjust(8, '0'), end="", flush=True)

                cmd = ["./" + example] + args
                start = time.time()
                p = subprocess.Popen(cmd, cwd=".", stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                out, eout = p.communicate(bytes(stdin, "utf-8"))
                ret = p.poll()
                end = time.time()

                if ret != 0:
                    print("  X   |")
                elif out.decode("ascii") != stdout:
                    print("  O   |")
                    print("{}: {} != {}".format(example, out, bytes(stdout, "ascii")), file=sys.stderr)
                elif eout.decode("ascii") != stderr:
                    print("  E   |")
                    print("{}: {} != {}".format(example, eout, bytes(stderr, "ascii")), file=sys.stderr)
                else:
                    print("{:02.2f} |".format(end - start).rjust(7, '0'))

                os.remove(example)

    horizontal()
